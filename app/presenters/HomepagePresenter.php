<?php

namespace App\Presenters;

use App\Controls\WineFoodFormControl;
use App\Utils\WineFoodPairing;
use Nette;
use App\Model;
use App\Model\WineManager;
use App\Controls\SearchFormControl;
use Nette\Application\UI\Multiplier;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
    /** @var \App\Model\WineManager @inject */
    public $wineModel;

    /** @var \App\Model\AccountManager @inject */
    public $accountManager;

    /** @var \App\Model\RecommendationManager @inject */
    public $recommendationModel;

    public function beforeRender()
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

	public function renderDefault()
	{
        if(!isset($this->template->wines)) {
            $this->template->wines = $this->wineModel->findAll();
        }
	}

    public function renderDetail($id)
    {
        $this->template->wine = $this->wineModel->findOne($id);
        $this->template->rating = $this->accountManager->getRating($this->getUser()->getId(), $id);
        $simSugarAcid = $this->recommendationModel->findSimilarWithAcidSugar($id, 4);
        $given = 6 - count($simSugarAcid);
        $simAlcoholPrice = $this->recommendationModel->findSimilarWithGivenVariety($id, $given);
        if (count($simSugarAcid) == 0) {
            $simSugarAcid = array();
        }
        $similar = array_merge($simSugarAcid, $simAlcoholPrice);
        $this->template->similar = $this->shuffle_assoc($similar);
    }

    public function renderRecommendation()
    {
        $this->template->rcmdsPearson = $this->recommendationModel->findRecommendedWinesPearson($this->getUser()->id, 4);
        $this->template->rcmdsCosine = $this->recommendationModel->findRecommendedWinesCosine($this->getUser()->id, 4);
        $this->template->rcmdsDistance = $this->recommendationModel->findRecommendedWinesDistance($this->getUser()->id, 4);
    }

    public function handleSimilarRecommended($method, $value)
    {
        $this->recommendationModel->insertSimilarRating($method, $value);
        $this->flashMessage('Tvé hodnocení bylo uloženo. Díky!');
        $this->redirect('this');
    }

    public function renderAccount()
    {
        $this->template->favourites = $this->accountManager->getFavourites($this->getUser()->getId());
    }

    public function renderRated()
    {
        $this->template->rated = $this->accountManager->getRated($this->getUser()->getId());
    }

    protected function createComponentSearchForm()
    {
        $form = new SearchFormControl($this->wineModel);
        $form['form']->onSuccess[] = function ($form) {
            $values = $form->getValues();
            $this->template->wines = $this->wineModel->findWinesWithAttributes($values['color'], $values['classification'], $values['variety']);
        };
        return $form;
    }

    protected function createComponentWineFoodForm()
    {
        $form = new WineFoodFormControl($this->wineModel);
        $form['form']->onSuccess[] = function ($form) {
            $values = $form->getValues();
            $wineFoodPairing = new WineFoodPairing();
            $ids = $wineFoodPairing->getVarietiesForFood($values['food']);
            $this->template->wines = $this->wineModel->findWinesVariety($ids);
        };
        return $form;
    }

    public function handleAddFavourite($id)
    {
        try {
            $this->accountManager->addFavourite($this->getUser()->getId(), $id);
            $this->flashMessage('Uloženo mezi oblíbené.');
            $this->redirect('this');
        } catch (\DibiDriverException $ex) {
            $this->flashMessage('Víno již máte mezi oblíbenými.');
            $this->redirect('this');
        }
    }

    public function handleRemoveFavourite($wineId)
    {
        $this->accountManager->deleteFavourite($this->getUser()->getId(), $wineId);
        $this->flashMessage('Víno bylo odstraněno z oblíbených.');
        $this->redirect('this');
    }

    public function handleSimilar($thisWine, $method, $value)
    {
        $this->recommendationModel->insertSimilarRating($method, $value);
        $this->redirect('Homepage:Detail', $thisWine);
    }

    protected function createComponentRatingForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addText('rating');
        $form->addHidden('wine');
        $form->addSubmit('submit', 'Uložit hodnocení');
        $form->onSuccess[] = function ($form) {
            $values = $form->getValues();
            try {
                $this->accountManager->addRating($this->getUser()->id, $values['wine'], $values['rating']);
            } catch (\DibiDriverException $ex) {
                $this->accountManager->updateRating($this->getUser()->id, $values['wine'], $values['rating']);
            }
            $this->redirect('this');
        };
        return $form;
    }

    private function shuffle_assoc($list) {
    if (!is_array($list)) return $list;

    $keys = array_keys($list);
    shuffle($keys);
    $random = array();
    foreach ($keys as $key) {
        $random[$key] = $list[$key];
    }
    return $random;
}
}
