<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\UserManager;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{
    /** @var UserManager @inject **/
    public $userManager;

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = new Nette\Application\UI\Form;
		$form->addText('username', 'Username:')
			->setRequired('Uživatelské jméno je povinný údaj.');

		$form->addPassword('password', 'Password:')
			->setRequired('Heslo je povinný údaj.');

		$form->addCheckbox('remember', 'Chci zůstat přihlášen');

		$form->addSubmit('send', 'Sign in');

		// call method signInFormSucceeded() on success
		$form->onSuccess[] = $this->signInFormSucceeded;
		return $form;
	}

	public function signInFormSucceeded($form)
	{
        $values = $form->getValues();

		if ($values->remember) {
			$this->getUser()->setExpiration('14 days', FALSE);
		} else {
			$this->getUser()->setExpiration('20 minutes', TRUE);
		}

		try {
			$this->getUser()->login($values->username, $values->password);
			$this->redirect('Homepage:');

		} catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Přihlašovací údaje jsou neplatné, zkuste to prosím znovu.');
		}
	}

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl jste úspšně odhlášen.');
		$this->redirect('in');
	}

    /**
     * Sign-up form factory.
     * @return Nette\Application\UI\Form
     */
    public function createComponentSignUpForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->addText('username', 'Username:')
            ->setRequired('Uživatelské jméno je povinný údaj.');

        $form->addPassword('password', 'Password:')
            ->setRequired('Heslo je povinný údaj.');

        $form->addSubmit('send', 'Save');

        $form->onSuccess[] = $this->signUpFormSucceeded;
        return $form;
    }

    public function signUpFormSucceeded($form, $values)
    {
        try {
            $this->userManager->add($values->username, $values->username);
            $this->redirect('Sign:Up');
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Při registraci došlo k chybě. Zkuste to prosím znovu.');
        }
    }

}
