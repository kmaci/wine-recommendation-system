<?php

namespace App\Presenters;

use Nette;
use App\Model;

class RecommendationPresenter extends BasePresenter
{
    /** @var \App\Model\RecommendationManager @inject */
    public $recommendationModel;


    public function renderSimilar($id)
    {
        $simSugarAcid = $this->recommendationModel->findSimilarWithAcidSugar($id, 4);
        $given = 6 - count($simSugarAcid);
        $simAlcoholPrice = $this->recommendationModel->findSimilarWithGivenVariety($id, $given);
        $this->template->similar = array_merge($simSugarAcid, $simAlcoholPrice);
    }
}
