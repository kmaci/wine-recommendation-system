<?php

namespace App\Model;

use Nette;

class RecommendationManager
{
    /** @var \DibiConnection */
    private $dibi;

    public function __construct(\DibiConnection $dibi)
    {
        $this->dibi = $dibi;
    }

    /**
     * Returns wines with given variety and similar price and amount of alcohol.
     * @param $id
     * @param $numberOfElements
     * @return array
     */
    public function findSimilarWithGivenVariety($id, $numberOfElements)
    {
        $wine = $this->dibi->select('*')
            ->from('wine')
            ->where('id = %i', $id)
            ->fetch();

        $variety = $wine->varietyId;

        $winesWithVariety = $this->dibi->select('*')
            ->from('wine')
            ->where('varietyId = %i', $variety)
            ->fetchAll();

        $givenWineVector = [$wine->price, $wine->alcohol];

        $sims = array();

        foreach ($winesWithVariety as $w) {
            $vector = [$w->price, $w->alcohol];

            $numerator = $this->dotProduct($vector, $givenWineVector);
            $denominator = $this->magnitude($vector) * $this->magnitude($givenWineVector);

            $sim = $numerator / $denominator;

            $wineWithSimilarity = array();
            $wineWithSimilarity['id'] = $w->id;
            $wineWithSimilarity['sim'] = $sim;

            if ($w->id != $id) {
                array_push($sims, $wineWithSimilarity);
            }
        }

        $output = array_slice($this->multid_sort($sims, 'sim'), 0, $numberOfElements);
        $ids = array();

        foreach ($output as $o) {
            array_push($ids, $o['id']);
        }

        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price, "1" as method
            ')
            ->from('wine')
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('wine.id IN (%i)', $ids)->fetchAll();

        return $ret;
    }

    public function findSimilarWithAcidSugar($id, $numberOfElements)
    {
        $wine = $this->dibi->select('*')
            ->from('wine')
            ->where('id = %i', $id)
            ->fetch();

        if (!$wine->acid || !$wine->sugar) {
            return;
        }

        $givenWineVector = [$wine->acid*10, $wine->sugar*10, $wine->alcohol];

        $winesWithAcidAndSugar = $this->dibi->select('*')
            ->from('wine')
            ->where('acid IS NOT NULL')
            ->and('sugar IS NOT NULL')
            ->fetchAll();

        $sims = array();

        foreach ($winesWithAcidAndSugar as $w) {
            $vector = [$w->acid, $w->sugar];

            $numerator = $this->dotProduct($vector, $givenWineVector);
            $denominator = $this->magnitude($vector) * $this->magnitude($givenWineVector);

            $sim = $numerator / $denominator;

            $wineWithSimilarity = array();
            $wineWithSimilarity['id'] = $w->id;
            $wineWithSimilarity['sim'] = $sim;

            if ($w->id != $id) {
                array_push($sims, $wineWithSimilarity);
            }
        }

        $output = array_slice($this->multid_sort($sims, 'sim'), 0, $numberOfElements);
        $ids = array();

        foreach ($output as $o) {
            array_push($ids, $o['id']);
        }

        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price, "2" as method
            ')
            ->from('wine')
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('wine.id IN (%i)', $ids)->fetchAll();

        return $ret;
    }

    public function insertSimilarRating($method, $rating)
    {
        $this->dibi->insert('similar_rating', array('method' => $method, 'rating' => $rating))->execute();
    }

    private function dotProduct($vector1, $vector2)
    {
        return array_sum(array_map('bcmul', $vector1, $vector2));
    }

    private function magnitude($vector)
    {
        $sum = 0;
        foreach ($vector as $v) {
            $sum += $v*$v;
        }

        return sqrt($sum);
    }

    private function multid_sort($arr, $index) {
        $b = array();
        $c = array();
        foreach ($arr as $key => $value) {
            $b[$key] = $value[$index];
        }

        asort($b);

        foreach ($b as $key => $value) {
            $c[] = $arr[$key];
        }

        return array_reverse($c);
    }

    public function getWinesWithRatings()
    {
        $ratings = $this->dibi->select('userId')
            ->setFlag('distinct')
            ->from('rating')
            ->fetchAll();

        $ratArray = array();

        foreach ($ratings as $r) {
            $rat = $this->dibi->select('*')->from('rating')
                ->where('userId = %i', $r)
                ->fetchPairs('wineId', 'rating');

            $ratArray[$r['userId']] = $rat;
        }

        return $ratArray;
    }

    /**
     * http://www.codediesel.com/php/item-based-collaborative-filtering-php/
     */
    private function similarityDistance($preferences, $person1, $person2)
    {
        $similar = array();
        $sum = 0;

        if(!isset($preferences[$person1])) {
            return;
        }

        foreach($preferences[$person1] as $key=>$value)
        {
            if(array_key_exists($key, $preferences[$person2]))
                $similar[$key] = 1;
        }

        if(count($similar) == 0)
            return 0;

        foreach($preferences[$person1] as $key=>$value)
        {
            if(array_key_exists($key, $preferences[$person2]))
                $sum = $sum + pow($value - $preferences[$person2][$key], 2);
        }

        return  1/(1 + sqrt($sum));
    }

    private function cosineSimilarity($preferences, $person1, $person2)
    {
        //if (!isset($preference[$person1])) return;

        $numerator = $this->dotProduct($preferences[$person1], $preferences[$person2]);
        $denominator = $this->magnitude($preferences[$person1]) * $this->magnitude($preferences[$person2]);

        $sim = $numerator / $denominator;

        return $sim;

    }

    /**
     * http://phpir.com/simple-collaborative-filtering/
     */
    public function pearson($preference, $person1, $person2)
    {
        $n = $sum1 = $sum2 = $sumSq1 = $sumSq2 = $product = 0;

        if (!isset($preference[$person1])) return;

        $item1ratings = $preference[$person1];
        $item2ratings = $preference[$person2];

        foreach($item1ratings as $user => $score) {
            if(!isset($item2ratings[$user])) {
                continue;
            }

            $n++;
            $sum1 += $score;
            $sum2 += $item2ratings[$user];
            $sumSq1 += pow($score, 2);
            $sumSq2 += pow($item2ratings[$user], 2);
            $product += $score * $item2ratings[$user];
        }

        // No shared ratings, so the similarity is 0
        // May want to tweak this to have a different minimum
        if($n == 0) {
            return 0;
        }

        // Work out the actual score
        $num = $product - (($sum1* $sum2)/$n);
        $den = sqrt(($sumSq1-pow($sum1, 2) / $n) * ($sumSq2 - pow($sum2, 2)/$n));

        if($den == 0) {
            return 0;
        }

        return $num/$den;
    }

    public function matchItems($preferences, $person)
    {
        $score = array();

        foreach($preferences as $otherPerson=>$values)
        {
            if($otherPerson !== $person)
            {
                $sim = $this->similarityDistance($preferences, $person, $otherPerson);

                if($sim > 0)
                    $score[$otherPerson] = $sim;
            }
        }

        return $score;

    }

    public function getRecommendations($preferences, $person, $method, $count = 2)
    {
        $total = array();
        $simSums = array();
        $ranks = array();
        $sim = 0;

        foreach($preferences as $otherPerson=>$values)
        {
            if($otherPerson != $person)
            {
                if ($method == 'pearson') {
                    $sim = $this->pearson($preferences, $person, $otherPerson);
                } elseif ($method == 'cosine') {
                    $sim = $this->cosineSimilarity($preferences, $person, $otherPerson);
                } elseif ($method == 'euclidean') {
                    $sim = $this->similarityDistance($preferences, $person, $otherPerson);
                }
            }

            if($sim > 0)
            {
                foreach($preferences[$otherPerson] as $key=>$value)
                {
                    if(!array_key_exists($key, $preferences[$person]))
                    {
                        if(!array_key_exists($key, $total)) {
                            $total[$key] = 0;
                        }
                        $total[$key] += $preferences[$otherPerson][$key] * $sim;

                        if(!array_key_exists($key, $simSums)) {
                            $simSums[$key] = 0;
                        }
                        $simSums[$key] += $sim;
                    }
                }

            }
        }

        foreach($total as $key=>$value)
        {
            $ranks[$key] = $value / $simSums[$key];
        }

        uasort($ranks, function($a, $b) {
            if ($a == $b) {
                return 0;
            }

            return ($a > $b) ? -1 : 1;
        });

        return array_slice($ranks, 0, $count, true);

    }

    public function findRecommendedWinesPearson($userId, $count)
    {
        $rcmds = $this->getRecommendations($this->getWinesWithRatings(), $userId, 'pearson', $count);

        $ids = array();

        if (count($rcmds) == 0) {
            return;
        }

        foreach ($rcmds as $key => $value) {
            array_push($ids, $key);
        }

        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price, "3" as method
            ')
            ->from('wine')
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('wine.id IN (%i)', $ids)->fetchAll();

        return $ret;
    }

    public function findRecommendedWinesCosine($userId, $count)
    {
        $rcmds = $this->getRecommendations($this->getWinesWithRatings(), $userId, 'cosine', $count);

        $ids = array();

        if (count($rcmds) == 0) {
            return;
        }

        foreach ($rcmds as $key => $value) {
            array_push($ids, $key);
        }

        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price, "4" as method
            ')
            ->from('wine')
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('wine.id IN (%i)', $ids)->fetchAll();

        return $ret;
    }

    public function findRecommendedWinesDistance($userId, $count)
    {
        $rcmds = $this->getRecommendations($this->getWinesWithRatings(), $userId, 'euclidean', $count);

        $ids = array();

        if (count($rcmds) == 0) {
            return;
        }

        foreach ($rcmds as $key => $value) {
            array_push($ids, $key);
        }

        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price, "5" as method
            ')
            ->from('wine')
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('wine.id IN (%i)', $ids)->fetchAll();

        return $ret;
    }
}
