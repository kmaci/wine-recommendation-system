<?php

namespace App\Model;

use Nette;

/**
 * Users management.
 */
class AccountManager
{

    /** @var \DibiConnection */
    private $dibi;

    public function __construct(\DibiConnection $dibi)
    {
        $this->dibi = $dibi;
    }

    public function addFavourite($userId, $wineId)
    {
        $this->dibi->insert('favourite', array('userId' => $userId, 'wineId' => $wineId))->execute();
    }

    public function getFavourites($userId)
    {
        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification
            ')
            ->from('favourite')
            ->leftJoin('wine')->on('favourite.wineId = wine.id')
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('favourite.userId = %i', $userId)
            ->fetchAll();
        return $ret;
    }

    public function getRated($userId)
    {
        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification
            ')
            ->from('wine')
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->leftJoin('rating')->on('rating.wineId = wine.id')
            ->where('rating.userId = %i', $userId)
            ->fetchAll();
        return $ret;
    }

    public function deleteFavourite($userId, $wineId)
    {
        $this->dibi->delete('favourite')->where(array('userId' => $userId, 'wineId' => $wineId))->execute();
    }

    public function addRating($userId, $wineId, $rating)
    {
        $this->dibi->insert('rating', array('userId' => $userId, 'wineId' => $wineId, 'rating' => $rating))->execute();
    }

    public function updateRating($userId, $wineId, $rating)
    {
        $this->dibi->update('rating', array('rating' => $rating))
            ->where('userId = %i', $userId)
            ->and('wineId = %i', $wineId)
            ->execute();
    }

    public function getRating($userId, $wineId)
    {
        $ret = $this->dibi->select('rating')
            ->from('rating')
            ->where('userId = %i', $userId)
            ->and('wineId = %i', $wineId)
            ->fetchSingle();
        return $ret;
    }
}
