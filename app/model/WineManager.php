<?php

namespace App\Model;

use Nette;

/**
 * Users management.
 */
class WineManager
{
    const TABLE_NAME = 'wine';
    
    /** @var \DibiConnection */
    private $dibi;

    public function __construct(\DibiConnection $dibi)
    {
        $this->dibi = $dibi;
    }

    public function findAll()
    {
         $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price
            ')
            ->from(self::TABLE_NAME)
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->orderBy('RAND()')
            ->fetchAll();
        return $ret;
    }

    public function findOne($id)
    {
        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.colorId as colorId, wine.price as price, wine.varietyId as varietyId
            ')
            ->from(self::TABLE_NAME)
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('wine.id = %i', $id)
            ->fetchAll();
        return $ret[0];
    }

    public function getAllVarieties()
    {
        $ret = $this->dibi->select('*')->from('variety')->fetchPairs('id', 'name');
        return $ret;
    }

    public function getAllColors()
    {
        $ret = $this->dibi->select('*')->from('color')->fetchPairs('id', 'name');
        return $ret;
    }

    public function getAllClassifications()
    {
        $ret = $this->dibi->select('*')->from('classification')->fetchPairs('id', 'name');
        return $ret;
    }

    public function findWinesWithAttributes($color, $classification, $variety)
    {
        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price
            ')
            ->from(self::TABLE_NAME)
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId');
        if ($color != 0) {
            $ret->where('color.id = %i', $color);
        }

        if ($classification != 0) {
            $ret->where('classification.id = %i', $classification);
        }

        if ($variety != 0) {
            $ret->where('variety.id = %i', $variety);
        }

        $ret->fetchAll();
        return $ret;
    }

    public function findWinesVariety($variety)
    {
        $ret = $this->dibi->select('
                wine.id, wine.name, wine.alcohol, wine.sugar, wine.acid, wine.year, wine.town, wine.region,
                wine.country, variety.name as variety, color.name as color, classification.name as classification,
                wine.price as price
            ')
            ->from(self::TABLE_NAME)
            ->leftJoin('variety')->on('variety.id = wine.varietyId')
            ->leftJoin('classification')->on('classification.id = wine.classificationId')
            ->leftJoin('color')->on('color.id = wine.colorId')
            ->where('variety.id IN (%i)', $variety);

        return $ret;
    }
}
