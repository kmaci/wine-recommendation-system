<?php

namespace App\Utils;


class WineFoodPairing
{
    public function getVarietiesForFood($food)
    {
        // these are just testing values
        switch ($food) {
            // --- CHEESE ----
            // veltlinske zelene, sylvanske zelene, rulandske bile, rulandske sede, ryzlink rynsky, rulandske modre
            case 'emental':
                return [17, 15, 10, 11, 12, 26];
                break;
            // veltlinske zelene, neuburske, sauvignon, ryzlink rynsky, chardonnay, modry portugal, cabernet moravia, frankovka
            case 'eidam':
                return [17, 8, 14, 12, 3, 24, 19, 22];
                break;
            // neuburske, sauvignon, chardonnay, veltlinske zelene, modry portugal, cabernet moravia, frankovka
            case 'gouda':
                return [8, 14, 3, 17, 24, 19, 22];
                break;
            // sylvanske zelene, chardonnay
            case 'gruyere':
                return [3, 15];
                break;
            // cabernet moravia, neuburske
            case 'leerdamer':
                return [19, 8];
                break;
            // ryzlink vlassky
            case 'chedar':
                return [13];
                break;
            // ryzlink vlassky, rulandske sede, ryzlink rynsky, tramin, merlot
            case 'trapist':
                return [13, 11, 12, 16, 23];
                break;
            // sauvignon, chardonnay, aurelius
            case 'hard-sheep':
                return [14, 3, 2];
                break;
            // sauvignon, rulandske bile, chardonnay
            case 'hard-goat':
                return [14, 10, 3];
                break;
            // sylvánské zelené, muller thurgau,
            case 'fresh-goat-sheep':
                return [15, 6];
                break;
            // rulandske bile, rulandske sede
            case 'parmigiano-reggiano':
            case 'gran-moravia':
                return [10, 11];
                break;
            // rulandske sede, palava
            case 'smoked':
                return [11, 9];
                break;
            // muller thurgau, irsai oliver
            case 'fresh-fruit':
                return [5, 6];
                break;
            // palava, irsai-oliver, muskat, tramin
            case 'blue-cheese':
                return [9, 5, 7, 16];
                break;
            // rulandske modre, modry portugal, cabernet sauvignon, cabernet moravia, andre, zweigeltrebe, frankovka, svatovavrinecke, neronet, merlot
            case 'soft-white':
                return [26, 24, 19, 20, 18, 28, 22, 27, 25, 23];
                break;

            // --- MEAT ----
            // muller thurgau, veltlinske zelene, ryzlink vlassky, sylvanske zelene, rulandske bile, neuburske
            case 'poultry':
                return [6, 17, 13, 15, 10, 8];
                break;
            // svatovavrinecke, frankovka, modry portugal, rulandske modre, dornfelder, cabernet sauvignon, andre
            case 'turkey-duck-goose':
                return [27, 22, 24, 26, 21, 20, 18];
                break;
            // ryzlink vlassky, veltlinske zelene, modry portugal, zweigeltrebe, merlot, rulandske modre
            case 'beef':
                return [13, 17, 24, 28, 26];
                break;
            // rulandske sede, sauvignon, aurelius, ryzlink rynsky, chardonnay, modry portugal, zweigeltrebe
            case 'pork':
                return [11, 14, 2, 12, 3, 24, 28];
                break;
            // frankovka, cabernet sauvignon, andre, neronet
            case 'mutton':
                return [22, 20, 18, 25];
                break;
            // andre, dornfelder, cabernet moravia, rulandske modre
            case 'game':
                return [18, 21, 19, 26];
                break;
            // sauvignon, ryzlink rynsky
            case 'smoked-meat':
                return [14, 12];
                break;
            // chardonnay, rulandske bile, rulandske sede, ryzlink rynsky
            case 'sea-fish':
                return [3, 10, 11, 12];
                break;
            // neuburske, aurelius, ryzlink vlassky, sylvanske zelene
            case 'carp-pike-trout':
                return [8, 2, 13, 15];
                break;
            // ryzlink rynsky, ryzlink vlassky
            case 'salmon-cod':
                return [12, 13];
                break;
            // chardonnay, rulandske sede
            case 'seafood':
                return [3, 11];
                break;

            // --- OTHER FOOD ----
            // tramin cerveny, aurelius
            case 'asian-food':
                return [2, 16];
                break;
            // zweigeltrebe, frankovka, svatovavrinecke
            case 'pasta':
                return [28, 22, 27];
                break;
            // chardonnay, rulandske sede
            case 'pizza':
                return [3, 11];
                break;
            // muller thurgau, veltlinske zelene
            case 'vegetarian-vegetables':
                return [6, 17];
                break;
            // muller thurgau, sylvanske zelene, neuburske, ryzlink rynsky, ryzlink vlassky
            case 'cold-starter':
                return [6, 15, 8, 12, 13];
                break;
            // ryzlink vlassky, veltlinske zelene, chardonnay, palava
            case 'hot-starter':
                return [13, 17, 3, 9];
                break;
            // veltlinske zelene, neuburske, muller thurgau, sylvanske zelene, rulandske bile
            case 'soup':
                return [17, 8, 6, 15, 10];
                break;
            // tramin, sauvignon, ryzlink rynsky
            case 'dessert':
                return [16, 14, 12];
                break;
        }
    }
}