<?php

namespace App\Controls;

use Nette\Application\UI;
use App\Model\WineManager;

class WineFoodFormControl extends UI\Control
{
    /* @var \App\Model\WineManager */
    private $manager;

    public function __construct(WineManager $manager)
    {
        $this->manager = $manager;
    }

    public function createComponentForm()
    {
        $form = new UI\Form;

        $meat = array(
            'poultry' => 'drůběž',
            'turkey-duck-goose' => 'krůta, kachna, husa',
            'beef' => 'hovězí',
            'pork' => 'vepřové',
            'game' => 'zvěřina',
            'smoked-meat' => 'uzené maso',
            'sea-fish' => 'mořské ryby',
            'carp-pike-trout' => 'kapr, štika, pstruh',
            'salmon-cod' => 'losos, treska',
            'seafood' => 'mořské plody'
        );

        $cheese = array(
            'emental' => 'Ementál',
            'eidam' => 'Eidam',
            'gouda' => 'Gouda',
            'gruyere' => 'Gruyere',
            'leerdammer' => 'Leerdammer',
            'chedar' => 'trvdé sýry typu čedar (Cheseter, Cantal)',
            'trapist' => 'sýry typu trapist (Reblochon, Chester, Blaťácké zlato, Bel paese)',
            'hard-sheep' => 'tvrdé ovčí sýry (Pecorino, Manchego, Picodon, Kefalotiri, oštěpek)',
            'hard-goat' => 'tvrdé kozí sýry (Picodon, Crottin de Chavignol)',
            'fresh-goat-sheep' => 'čerstvé ovčí a kozí sýry (feta, brynza, balkánský sýr)',
            'parmigiano-reggiano' => 'Parmigiano reggiano',
            'gran-moravia' => 'Gran Moravia',
            'smoked' => 'uzené a pařené sýry (oštěpek, parenica)',
            'fresh-fruit' => 'čerstvé sýry s ovocem a ořechy',
            'blue-cheese' => 'sýry s modrou plísní (Bleu d\'Auvergne, Gorgonzola, Zlatá Niva)',
            'soft-white' => 'měkké sýry s bílou plísní (Camembert, Brie, Hermelín)'
        );

        $otherFood = array(
            'asian-food' => 'Asijská kuchyně',
            'pasta' => 'těstoviny',
            'pizza' => 'pizza',
            'vegetarian-vegetables' => 'vegetariánská jídla a zelenina',
            'cold-starter' => 'studené předkrmy',
            'hot-starter' => 'teplé předkrmy',
            'soup' => 'polévky',
            'dessert' => 'dezerty'
        );

        $select = array(
            'Maso' => $meat,
            'Sýry' => $cheese,
            'Ostatní jídlo' => $otherFood
        );

        $form->addSelect('food', 'Jídlo', $select);
        $form->addSubmit('send', 'Filtrovat');

        return $form;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/../templates/components/wineFoodForm.latte');
        $template->render();
    }

} 