<?php

namespace App\Controls;

use Nette\Application\UI;
use Nette\Object;
use App\Model\WineManager;

class SearchFormControl extends UI\Control
{
    /* @var \App\Model\WineManager */
    private $manager;

    public function __construct(WineManager $manager)
    {
        $this->manager = $manager;
    }

    public function createComponentForm()
    {
        $form = new UI\Form;

        $noValue[0] = 'Nezáleží';

        $colorsDtb = $this->manager->getAllColors();
        $form->addSelect('color', 'Barva', array_merge($noValue, $colorsDtb));

        $varietiesDtb = $this->manager->getAllVarieties();
        $form->addSelect('variety', 'Odrůda', array_merge($noValue, $varietiesDtb));

        $classificationDtb = $this->manager->getAllClassifications();
        $form->addSelect('classification', 'Odrůda', array_merge($noValue, $classificationDtb));

        $form->addSubmit('send', 'Odeslat');

        return $form;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/../templates/components/searchForm.latte');
        $template->render();
    }
}