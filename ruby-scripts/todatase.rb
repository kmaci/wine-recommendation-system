require 'pry'
require 'rubygems'
require 'open-uri'
require 'yaml'
require 'set'

def normalize(string)
	normalized = string.tr(
		"ÀÁÂÃÄÅàáâãäåĀāĂăĄąÇçĆćĈĉĊċČčÐðĎďĐđÈÉÊËèéêëĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħÌÍÎÏìíîïĨĩĪīĬĭĮįİıĴĵĶķĸĹĺĻļĽľĿŀŁłÑñŃńŅņŇňŉŊŋÒÓÔÕÖØòóôõöøŌōŎŏŐőŔŕŖŗŘřŚśŜŝŞşŠšſŢţŤťŦŧÙÚÛÜùúûüŨũŪūŬŭŮůŰűŲųŴŵÝýÿŶŷŸŹźŻżŽž",
		"AAAAAAaaaaaaAaAaAaCcCcCcCcCcDdDdDdEEEEeeeeEeEeEeEeEeGgGgGgGgHhHhIIIIiiiiIiIiIiIiIiJjKkkLlLlLlLlLlNnNnNnNnnNnOOOOOOooooooOoOoOoRrRrRrSsSsSsSssTtTtTtUUUUuuuuUuUuUuUuUuUuWwYyyYyYZzZzZz"
	).downcase
	return normalized
end

def getVarietyId(variety)
	normalized = normalize(variety)

	if normalized.include? 'agni'
		return '1'
	elsif normalized.include? 'aurelius'
		return '2'
	elsif normalized.include? 'chardonnay'
		return '3'
	elsif normalized.include? 'hibernal'
		return '4'
	elsif normalized.include? 'irsai oliver'
		return '5'
	elsif normalized.include? 'muller thurgau'
		return '6'
	elsif normalized.include? 'muskat moravsky'
		return '7'
	elsif normalized.include? 'neuburske'
		return '8'
	elsif normalized.include? 'palava'
		return '9'
	elsif normalized.include? 'rulandske bile'
		return '10'
	elsif normalized.include? 'rulandske sede'
		return '11'
	elsif normalized.include? 'ryzlink rynsky'
		return '12'
	elsif normalized.include? 'ryzlink vlassky'
		return '13'
	elsif normalized.include? 'sauvignon' and not normalized.include? 'cabernet'
		return '14'
	elsif normalized.include? 'sylvanske zelene'
		return '15'
	elsif normalized.include? 'tramin cerveny'
		return '16'
	elsif normalized.include? 'veltlinske zelene'
		return '17'
	elsif normalized.include? 'andre'
		return '18'
	elsif normalized.include? 'cabernet moravia'
		return '19'
	elsif normalized.include? 'cabernet sauvignon'
		return '20'
	elsif normalized.include? 'dornfelder'
		return '21'
	elsif normalized.include? 'frankovka'
		return '22'
	elsif normalized.include? 'merlot'
		return '23'
	elsif normalized.include? 'modry portugal'
		return '24'
	elsif normalized.include? 'neronet'
		return '25'
	elsif normalized.include? 'rulandske modre'
		return '26'
	elsif normalized.include? 'svatovavrinecke'
		return '27'
	elsif normalized.include? 'zweigeltrebe'
		return '28'
	else 
		return nil
	end
end

def getColorId(item)
	color = normalize(item)
	if color == 'bile'
		return '1'
	elsif color == 'cervene'
		return '2'
	elsif color == 'ruzove'
		return '3'
	else 
		return nil
	end
end

def getClassificationId(item)
	cls = normalize(item)
	if cls == 'stolni vino'
		return '1'
	elsif cls == 'jakostni vino'
		return '2'
	elsif cls == 'kabinetni vino'
		return '3'
	elsif cls == 'pozdni sber'
		return '4'
	elsif cls == 'vyber z hroznu'
		return '5'
	elsif cls == 'vyber z bobuli'
		return '6'
	elsif cls == 'vyber z cibeb'
		return '7'
	elsif cls == 'ledove vino'
		return '8'
	elsif cls == 'slamove vino'
		return '9'
	else
		return nil
	end
end

def getTypeId(type)
	tp = normalize(type)
	if tp == 'suche'
		return '1'
	elsif tp == 'polosuche'
		return '2'
	elsif tp == 'polosladke'
		return '3'
	elsif tp == 'sladke'
		return '4'
	else
		return nil
	end
end

def quotize(string)
	return '\'' << string << '\''
end

def getPrice(price)
	cena = price.sub(" Kč", "")
	return cena.sub(",", ".")
end

wines = YAML.load(File.read('wines.txt'))
string = 'INSERT INTO `wine`(`id`, `name`, `varietyId`, `colorId`, `classificationId`, `alcohol`, `sugar`, `acid`, `year`, `town`, `region`, `country`, `price`) VALUES '
wines.each do |item|
	if item['region'] and item['region'] == 'Morava'
		
		variety = getVarietyId(item['name'])

		price = quotize(getPrice(item['cena']))

		if variety == nil
			varienty = nil
		else
			variety = quotize(variety)
		end

		if item['name']
			name = quotize(item['name'])
		end

		if item['color']
			color = quotize(getColorId(item['color']))
		end

		if item['type']
			type = quotize(getTypeId(item['type']))
		end

		if item['classification']
			classification = getClassificationId(item['classification'])
			if classification == nil
				classification = nil
			else
				classification = quotize(classification)
			end
		end

		if item['alcohol']
			alcohol = item['alcohol'].sub(' %', '')
			alcohol = quotize(alcohol.sub(',', '.'))
		else
			alcohol = 'NULL'
		end

		if item['acid']
			acid = item['acid'].sub(',', '.')
			acid = quotize(acid.sub(' g/l', ''))
		else
			acid = 'NULL'
		end

		if item['sugar']
			sugar = item['sugar'].sub(',', '.')
			sugar = quotize(sugar.sub(' g/l', ''))
		else
			sugar = 'NULL'
		end

		if item['town']
			town = quotize(item['town'])
		else
			town = 'NULL'
		end

		year = item['year']
	end

	if name and variety and color and classification and year
		string << '(' << name << ', ' << variety << ', ' << color << ', ' << classification << ', ' << alcohol << ', ' << sugar << ', ' << acid << ', ' << year << ', ' << town << ', ' << '\'Morava\'' << ', ' << '\'Česká republika\'' << ', ' << price << ')'
		string << ', '
		string << "\n"
	end
end

File.open("dump.sql", "w+") { |file| file.write(string) }


