#!/usr/bin/ruby

require 'pry'
require 'rubygems'
require 'open-uri'
require 'nokogiri'
require 'yaml'
require 'set'

array = []

pinks = YAML.load(File.read('pinks.txt'))
reds = YAML.load(File.read('reds.txt'))
whites = YAML.load(File.read('whites.txt'))

array = whites + reds + pinks

File.open("wines.txt", "w"){|f| f.write(YAML.dump(array)) }

wines = YAML.load(File.read('wines.txt'))

pp wines