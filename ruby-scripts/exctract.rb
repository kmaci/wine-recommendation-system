#!/usr/bin/ruby

require 'pry'
require 'rubygems'
require 'open-uri'
require 'nokogiri'
require 'yaml'
require 'set'

# name
# color => white, red, pink
# type => sucha, polosucha atd. (Druh vína)
# classification => Klasifikace/přívlastek
# alcohol => Obsah alkoholu
# acid => Obsah kyselin
# sugar => Zbytkový cukr
# year => Ročník
# town => Vinařská obec
# region => Vinařská oblast
# country => Země původu
# price => Cena

array = []

Dir.foreach('whites') do |file|
  next if file == '.' or file == '..'
  file = Nokogiri::HTML(open('whites/' + file))

  hash = {}
	hash['name'] = file.css('h1 span').text
	hash['color'] = "Bílé"

	hash['cena'] = file.css('.priceShop td').text

	next unless tr_detail = file.css('.prodInfo')[1]
	tr_detail.css('tr').each do | item |
		th = item.css('th').text.strip
		if th == "Druh vína"
			hash['type'] = item.css('td').text
		elsif th == "Klasifikace/přívlastek"
			hash["classification"] = item.css('td').text
		elsif th == "Obsah alkoholu"
			hash["alcohol"] = item.css('td').text
		elsif th == "Ročník"
			hash["year"] = item.css('td').text
		elsif th == "Vinařská obec"
			hash["town"] = item.css('td').text
		elsif th == "Vinařská oblast"
			hash["region"] = item.css('td').text
		elsif th == "Země původu"
			hash["country"] = item.css('td').text
		elsif th == "Obsah kyselin"
			hash["acid"] = item.css('td').text
		elsif th == "Zbytkový cukr"
			hash["sugar"] = item.css('td').text
		end
	end

	array << hash
end

File.open("whites.txt", "w"){|f| f.write(YAML.dump(array)) }

=begin 

set = Set.new
array_keys = []

array.each do |item|
	item.keys.each do |key|
		array_keys << key
	end
end

counts = Hash.new 0

array_keys.each do |word|
	counts[word] += 1
end

pp counts



#overview = Nokogiri::HTML(open("http://www.winehouse.cz/produkty/vina/bila-vina?limit=600&zobrazeni=obr&ord=akce&smer=DESC"))
overview = Nokogiri::HTML(open("overview.html"))


array = []
overview.css('.prodListItem').each do | product |
	detail_link = product.css('a')[0]["href"]
	detail = Nokogiri::HTML(open("detail.html"))

	hash = {}
	hash['name'] = detail.css('h1 span').text
	next unless tr_detail = detail.css('.prodInfo')[1]
	tr_detail.css('tr').each do | item |
		hash[item.css('th').text.strip] = item.css('td').text
	end

	array << hash

end

File.open("wines", "w"){|f| f.write(YAML.dump(array)) }
=end