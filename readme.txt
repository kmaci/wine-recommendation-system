Wine Recommendation System
=============
1. Install vendors (php composer.phar install)
2. Create database schema (app/model/wine_recommendation_system.sql)
3. Start server from root folder (php -S localhost:8888 -t www)

http://winerecommendationsystem.8u.cz/www/
username: test
password: test